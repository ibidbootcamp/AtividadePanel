﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Cadastro.aspx.cs" Inherits="CadastroWebForms.Cadastro" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Cadastro</title>
    <link href="StyleSheet1.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
<asp:Table runat="server">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="panelInfoPessoais">
                <h2>Informações Pessoais</h2>
                Nome: <asp:TextBox ID="txtNome" runat="server"></asp:TextBox><br />
                Sobrenome: <asp:TextBox ID="txtSobrenome" runat="server"></asp:TextBox><br />
                Gênero: <asp:TextBox ID="txtGenero" runat="server"></asp:TextBox><br />
                Celular: <asp:TextBox ID="txtCelular" runat="server"></asp:TextBox><br />
                <asp:Button ID="btnProximo1" runat="server" Text="Próximo" OnClick="btnProximo1_Click" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="panelDetalhesEndereco" Visible="false">
                <h2>Detalhes do Endereço</h2>
                Endereço: <asp:TextBox ID="txtEndereco" runat="server"></asp:TextBox><br />
                Cidade: <asp:TextBox ID="txtCidade" runat="server"></asp:TextBox><br />
                CEP: <asp:TextBox ID="txtCEP" runat="server"></asp:TextBox><br />
                <asp:Button ID="btnVoltar2" runat="server" Text="Voltar" OnClick="btnVoltar2_Click" />
                <asp:Button ID="btnProximo2" runat="server" Text="Próximo" OnClick="btnProximo2_Click" />
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Panel runat="server" ID="panelAreaLogin" Visible="false">
                <h2>Área de Login</h2>
                Usuário: <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox><br />
                Senha: <asp:TextBox ID="txtSenha" runat="server" TextMode="Password"></asp:TextBox><br />
                <asp:Button ID="btnVoltar3" runat="server" Text="Voltar" OnClick="btnVoltar3_Click" />
                <asp:Button ID="btnEnviar" runat="server" Text="Enviar" OnClick="btnEnviar_Click" /><br />
                <asp:Label ID="lblMensagem" runat="server" Text="" ForeColor="Red"></asp:Label>
            </asp:Panel>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>

    </form>
</body>
</html>
