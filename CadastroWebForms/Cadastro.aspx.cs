﻿using System;

namespace CadastroWebForms
{
    public partial class Cadastro : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                panelInfoPessoais.Visible = true;
                panelDetalhesEndereco.Visible = false;
                panelAreaLogin.Visible = false;
                lblMensagem.Visible = false;
            }
        }

        protected void btnProximo1_Click(object sender, EventArgs e)
        {
            panelInfoPessoais.Visible = false;
            panelDetalhesEndereco.Visible = true;
            panelAreaLogin.Visible = false;
        }

        protected void btnProximo2_Click(object sender, EventArgs e)
        {
            panelInfoPessoais.Visible = false;
            panelDetalhesEndereco.Visible = false;
            panelAreaLogin.Visible = true;
        }

        protected void btnVoltar2_Click(object sender, EventArgs e)
        {
            panelInfoPessoais.Visible = true;
            panelDetalhesEndereco.Visible = false;
            panelAreaLogin.Visible = false;
        }

        protected void btnVoltar3_Click(object sender, EventArgs e)
        {
            panelInfoPessoais.Visible = false;
            panelDetalhesEndereco.Visible = true;
            panelAreaLogin.Visible = false;
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            panelInfoPessoais.Visible = false;
            panelDetalhesEndereco.Visible = false;
            panelAreaLogin.Visible = true;
            lblMensagem.Visible = true;
            lblMensagem.Text = "AVISO! Os seus dados foram enviados com sucesso.";
        }
    }
}
